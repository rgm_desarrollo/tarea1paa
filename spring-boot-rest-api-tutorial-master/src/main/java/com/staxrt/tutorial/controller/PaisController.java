package com.staxrt.tutorial.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.staxrt.tutorial.model.pais;
import com.staxrt.tutorial.service.PaisService;

@RestController
public class PaisController {

	
	@GetMapping("/Saludo")
	public String Saludo()
	{
		return  "hola mundo";
	}

	@Autowired
	private PaisService servicio;
	

	@GetMapping("/paises")	
	public List<pais>findAllPais()
	{
		return servicio.obtenerPais();
	}
	
	   @PostMapping("/addPaises")
	    public pais addPais(@RequestBody pais pais) {
	        return servicio.savePais(pais);
	    }
	
	
	@GetMapping("/pais/{id}")	
	public pais finddpaisbyId(@PathVariable long id)
	{		
		return servicio.obtenerIdPais(id);
	}	
	

	@PutMapping("/pais/update")
	public pais updatePais(@RequestBody pais paisser)
	{		
		return servicio.updatePais(paisser);
	}
	
	
	 @DeleteMapping("/pais/delete/{id}")
	    public String deletePais(@PathVariable long id) {
	        return servicio.deletePais(id);
	    }
	 
	
	
	
}
