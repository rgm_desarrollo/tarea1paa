package com.staxrt.tutorial.model;

import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.springframework.data.jpa.domain.support.AuditingEntityListener;

@Entity
@Table(name = "departamento")
@EntityListeners(AuditingEntityListener.class)
public class departamento {
	@Id
    @GeneratedValue(strategy = GenerationType.AUTO)
	private long id;
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public departamento getDepartamento() {
		return departamento;
	}
	public void setDepartamento(departamento departamento) {
		this.departamento = departamento;
	}
	public int getCodigo() {
		return codigo;
	}
	public void setCodigo(int codigo) {
		this.codigo = codigo;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public int getNumero_muncipios() {
		return numero_muncipios;
	}
	public void setNumero_muncipios(int numero_muncipios) {
		this.numero_muncipios = numero_muncipios;
	}
	public int getPoblacion() {
		return poblacion;
	}
	public void setPoblacion(int poblacion) {
		this.poblacion = poblacion;
	}
	@ManyToOne
    @JoinColumn(name = "fkid_departamento", nullable = false, updatable = false)
    private departamento departamento;
	
	private int codigo;
	private String nombre;
	private int numero_muncipios;
	private int poblacion;
	
	

}
