package com.staxrt.tutorial.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.staxrt.tutorial.model.departamento;
import com.staxrt.tutorial.repository.DepartamentoRepository;

@Service
public class DepartamentoService {
	
	@Autowired
	private DepartamentoRepository repository;

	public departamento saveDepartamento(departamento departament)
	{
		return repository.save(departament);
		
	}
	
	public List<departamento> saveDepartamentos(List<departamento> departamentos)
	{
		return repository.saveAll(departamentos);
	}
	
	public List<departamento> obtenerDepartamentos()
	{
		return repository.findAll();
	}
	
	public departamento obtenerIdDepartamento(long id)
	{
		return repository.findById(id).orElse(null);
	}
	
	public String deleteDepartamento(long id)
	{
		repository.deleteById(id);
		return "Departamento eliminado";
	}
	
	public departamento updateDepartamento(departamento depar)
	{
		departamento d = repository.findById(depar.getId()).orElse(null);
		d.setNombre(depar.getNombre());
		d.setCodigo(depar.getCodigo());
		d.setNumero_muncipios(depar.getNumero_muncipios());
		d.setPoblacion(depar.getPoblacion());
		return repository.save(d);
		
		
	}
	
	

}
