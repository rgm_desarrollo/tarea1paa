package com.staxrt.tutorial.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.GetMapping;

import com.staxrt.tutorial.model.departamento;
import com.staxrt.tutorial.model.pais;
import com.staxrt.tutorial.repository.DepartamentoRepository;
import com.staxrt.tutorial.repository.PaisRepository;

@Service
public class PaisService {
	
	
	@Autowired
	private PaisRepository repository;

	public pais savePais(pais pais)
	{
		return repository.save(pais);
		
	}
	
	 @GetMapping("/saludo")
	    public String saludo()
	    {
	    	return "hola mundo";
	    }
	
	public List<pais> savePais(List<pais> pais)
	{
		return repository.saveAll(pais);
	}
	
	public List<pais> obtenerPais()
	{
		return repository.findAll();
	}
	
	public pais obtenerIdPais(long id)
	{
		return repository.findById(id).orElse(null);
	}
	
	public String deletePais(long id)
	{
		repository.deleteById(id);
		return "Pais eliminado";
	}
	
	public pais updatePais(pais pais)
	{
	
		pais p = repository.findById(pais.getId()).orElse(null);
		p.setCodigoiso(pais.getCodigoiso());
		p.setContinente(pais.getContinente());
		p.setNombre(pais.getNombre());
		
			
		return repository.save(p);
		
		
	}
	
	

}
