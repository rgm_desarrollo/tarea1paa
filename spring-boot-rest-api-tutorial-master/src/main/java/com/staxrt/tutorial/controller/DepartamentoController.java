package com.staxrt.tutorial.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.staxrt.tutorial.model.departamento;
import com.staxrt.tutorial.service.DepartamentoService;

@RestController
public class DepartamentoController {
	
	@Autowired
	private DepartamentoService servicio;
	
	@GetMapping("/pais//departamentos")	
	public List<departamento>findAllDepartamentos()
	{
		return servicio.obtenerDepartamentos();
	}
	
	@GetMapping("/pais/departamento/{id}")	
	public departamento finddepartamentobyid(@PathVariable long id)
	{		
		return servicio.obtenerIdDepartamento(id);
	}	
	
	
	@PutMapping("/pais/update")
	public departamento updateDepartamento(@RequestBody departamento depart)
	{		
		return servicio.updateDepartamento(depart);
	}
		
	
	 @DeleteMapping("/pais/delete/{id}")
	    public String deleteDepartamento(@PathVariable long id) {
	        return servicio.deleteDepartamento(id);
	    }

}
