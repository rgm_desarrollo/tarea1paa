package com.staxrt.tutorial.model;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.springframework.data.jpa.domain.support.AuditingEntityListener;

@Entity
@Table(name = "pais")
@EntityListeners(AuditingEntityListener.class)
public class pais {
	
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	
	
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "id")
    private List<departamento> departamento;
	
    
   public List<departamento> getDepartamento() {
		return departamento;
	}
	public void setDepartamento(List<departamento> departamento) {
		this.departamento = departamento;
	}		
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getCodigoiso() {
		return codigoiso;
	}
	public void setCodigoiso(String codigoiso) {
		this.codigoiso = codigoiso;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getContinente() {
		return continente;
	}
	public void setContinente(String continente) {
		this.continente = continente;
	}
	private String codigoiso;
	private String nombre;
	private String continente;

	
	

}
