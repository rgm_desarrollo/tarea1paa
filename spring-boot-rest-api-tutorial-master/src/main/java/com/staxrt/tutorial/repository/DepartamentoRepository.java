package com.staxrt.tutorial.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.staxrt.tutorial.model.departamento;


public interface DepartamentoRepository   extends JpaRepository<departamento, Long>{

}
