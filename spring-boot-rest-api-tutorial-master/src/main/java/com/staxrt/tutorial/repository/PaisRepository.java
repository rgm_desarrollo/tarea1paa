package com.staxrt.tutorial.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.staxrt.tutorial.model.pais;

public interface PaisRepository  extends JpaRepository<pais, Long>{

}
